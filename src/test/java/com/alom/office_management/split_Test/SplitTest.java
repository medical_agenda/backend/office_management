package com.alom.office_management.split_Test;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;


import com.alom.office_management.split.Split;

public class SplitTest {
	
	private Split split = new Split();
	
	@Test
	public void testSplitByZero() {
		Integer divident=2;
		Integer divider=0;
		Integer result = this.split.split(divident, divider);
		assertNull(result,"Divider is 0");
	}
	
	@Test
	public void testSplitNullDivident() {
		Integer divident=null;
		Integer divider=0;
		Integer result = this.split.split(divident, divider);
		assertNull(result,"Divident is null");
	}
	
	@Test
	public void testSplitNullDivider() {
		Integer divident=2;
		Integer divider=null;
		Integer result = this.split.split(divident, divider);
		assertNull(result,"Divider is null");
	}
	@Test
	public void testSplitNullDividentAndDivider() {
		Integer divident=null;
		Integer divider=null;
		Integer result = this.split.split(divident, divider);
		assertNull(result,"Divident and divider are null");
	}
	@Test
	public void testSplitResultExpectedEqualsResul() {
		Integer divident=10;
		Integer divider=7;
		Integer resultExpected=1;
		Integer result = this.split.split(divident, divider);
		assertEquals(resultExpected,result,"The result expected is different from result obtain");
	}
	@Test
	public void testSplitNegativeDivident() {
		Integer divident=-10;
		Integer divider=7;
		Integer resultExpected=-1;
		Integer result = this.split.split(divident, divider);
		assertEquals(resultExpected,result,"testSplitResultExpectedTrue");
	}
	@Test
	public void testSplitNegativeDivisor() {
		Integer divident=10;
		Integer divider=-7;
		Integer resultExpected=-1;
		Integer result = this.split.split(divident, divider);
		assertEquals(resultExpected,result,"testSplitResultExpectedTrue");
	}
	public void testSplitNegativeDividentAndDivisor() {
		Integer divident=-10;
		Integer divider=-7;
		Integer resultExpected=-1;
		Integer result = this.split.split(divident, divider);
		assertEquals(resultExpected,result,"testSplitResultExpectedTrue");
	}
}
