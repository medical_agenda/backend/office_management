package com.alom.office_management.chainsTest;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.alom.office_management.chains.ChainImpl;
import com.alom.office_management.chains.ConvertChainImpl;

@ExtendWith(MockitoExtension.class)
public class ChainImplTest {
	
	@Mock
	private ConvertChainImpl convertChainImpl;
	
	@InjectMocks
	private ChainImpl chainImpl;
	
	@Test
	public void testIsPalindromeTrue() {
		String chainInput= "reconOcer";
		String chainConverted= "RECONOCER";
		Mockito.lenient().when(convertChainImpl.convert(chainInput)).thenReturn(chainConverted);
		boolean result = chainImpl.isPalindrome(chainInput);
		assertTrue(result,"The chain is palindrome");
	}
	
	@Test
	public void testIsPalindromeFalse() {
		String chainInput= "reconAcer";
		String chainConverted= "RECONACER";
		Mockito.lenient().when(convertChainImpl.convert(chainInput)).thenReturn(chainConverted);
		boolean result = chainImpl.isPalindrome(chainInput);
		assertFalse(result,"The chain is not palindrome");
	}
	
	@Test
    public void testPalindromeNullchainInput() {
		String chainInput= null;
		String chainConverted= null;
		Mockito.lenient().when(convertChainImpl.convert(chainInput)).thenReturn(chainConverted);
		boolean result = chainImpl.isPalindrome(chainInput);
        
        assertFalse(result, "Null chain should not be considered a palindrome");
    }
	
	
}
