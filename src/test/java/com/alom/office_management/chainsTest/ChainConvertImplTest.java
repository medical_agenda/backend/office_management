package com.alom.office_management.chainsTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.alom.office_management.chains.ConvertChainImpl;

public class ChainConvertImplTest {
	
	@Test
	public void convert() {
		ConvertChainImpl convertChainImpl = new ConvertChainImpl();
		String expected = "RECONOCER";
		String result = convertChainImpl.convert("Reconocer");
		assertEquals(expected,result);
	}

}
