package com.alom.office_management.exception.handler;

import java.net.URI;
import java.time.Instant;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.alom.office_management.exception.OfficeNotFoundException;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(OfficeNotFoundException.class)
	ProblemDetail handleBookmarkNotFoundException(OfficeNotFoundException e) {
		ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND,e.getMessage());
		problemDetail.setTitle("Consultorio Not Found");
		problemDetail.setType(URI.create("http://localhost:8092/not-found"));
		problemDetail.setProperty("errorCategory", "generic");
		problemDetail.setProperty("timestamp", Instant.now());
		return problemDetail;
	}
}
