package com.alom.office_management.model;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Entity(name ="OFFICE")
@Data
@NoArgsConstructor
public class OfficeEntity {
	
	@NonNull
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "OFFID")
	private Integer id;
	
	@NonNull
	@Column(name = "OFFNAME")
	private String name;
	
	@Enumerated(value = EnumType.STRING)
	@Column(name = "OFFSTATUS")
	private OfficeStatusEnum status;
	
	@Column(name = "OFFFLOOR")
	private int floor;
	
	@Column(name = "OFFACCESSIBILITY")
	private Boolean accessibility;
	
	@Column(name = "OFFDESCRIPTION")
	private String description;
	
	@Column(name = "OFFCREATIONDATE")
	private LocalDate creationDate;
	
	@Column(name = "OFFCREATIONUSER")
	private Long creationUser;
}



