package com.alom.office_management.model;

import org.mapstruct.Mapper;

@Mapper(componentModel ="spring")
public interface OfficeMapper {
	//public OfficeDTO mapOffice(OfficeEntity officeEntity);
	public OfficeDTO mapOffice(OfficeEntity officeEntity);
}
