package com.alom.office_management.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NonNull;

@Data
@JsonPropertyOrder({"id","name","status","floor","accessibility","description", "creationDate","creationUser"})
public class OfficeDTO {
	
	@NonNull
	private Integer id;
	
	@NonNull
	private String name;
	private OfficeStatusEnum status;  //https://www.baeldung.com/spring-boot-enum-mapping
	private int floor;
	private Boolean accessibility;
	private String description;
	private LocalDate creationDate;
	private Long creationUser;
}
