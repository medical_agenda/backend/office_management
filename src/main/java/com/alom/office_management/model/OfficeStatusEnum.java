package com.alom.office_management.model;

public enum OfficeStatusEnum {
	DIS("enum.consultorioStatus.disponible"),
	NOD("enum.consultorioStatus.noDisponible");
	
	//Atributo que contiene la clave del mensaje para la internacionalizacion  
	private String messageCode;

	/**
	 * Constructor que recibe como parametro el codigo del mensaje
	 * @param codigoMensaje, Clave del mensaje para para internacionalizacion
	*/
	OfficeStatusEnum(String messageCode) {		
		this.messageCode = messageCode;
	}

	public String getmessageCode() {
		return messageCode;
	}

	/***
	 * Metodo encargado de retornar el enum segun su nombre
	 * @param description
	 * @return
	 */
	public static OfficeStatusEnum getEnumValue(String description) {
		if (description != null) {
			for (OfficeStatusEnum consultorioStatusEnum : OfficeStatusEnum.values()) {
				if (description.equalsIgnoreCase(consultorioStatusEnum.name())) {
					return consultorioStatusEnum;
				}
			}
		}
		return null;
	}
}
