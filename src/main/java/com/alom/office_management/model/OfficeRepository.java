package com.alom.office_management.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alom.office_management.response.ResponseDTO;

@Repository
public interface OfficeRepository extends JpaRepository<OfficeEntity, Integer> {
	
	public List<OfficeEntity> findAllOfficeByCreationUser(Long creationUser);
	public List<OfficeEntity> findAllOfficeByFloor(int Floor);
	public Boolean existsByName(String name);
//	public ResponseDTO getOfficeByIdMorePerson(Integer id);
 
}
