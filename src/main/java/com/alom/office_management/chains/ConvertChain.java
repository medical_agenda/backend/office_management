package com.alom.office_management.chains;

public interface ConvertChain {
	
	public abstract String convert(String chain);

}
