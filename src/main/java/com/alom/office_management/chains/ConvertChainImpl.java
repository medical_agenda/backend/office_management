package com.alom.office_management.chains;

import org.springframework.stereotype.Service;

import lombok.Data;
import lombok.NoArgsConstructor;

@Service
public class ConvertChainImpl implements ConvertChain {
	
	@Override
	public String convert(String chain) {
		if (chain == null) {
            return null;
        }
        return chain.replaceAll("\\s+", "").toUpperCase();
	}
}
