package com.alom.office_management.chains;

public interface Chain {
	public boolean isPalindrome(String chain);
}
