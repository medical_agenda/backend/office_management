package com.alom.office_management.chains;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChainImpl implements Chain{
	
	@Autowired
	private ConvertChainImpl convertChainImpl;
	
	@Override
	public boolean isPalindrome(String chain) {
		if (chain == null) {
            return false;
        }
		
		convertChainImpl= new ConvertChainImpl();
		String newChain = convertChainImpl.convert(chain);
		if (newChain == null) {
            return false;
        }
		for (int x=0, size=newChain.length()-1 ; x<newChain.length(); x++, size--) {
			if (newChain.charAt(x)!=newChain.charAt(size)) {
				return false;
			}
		}
		return true;
	}
}
