package com.alom.office_management.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alom.office_management.model.OfficeEntity;
import com.alom.office_management.model.OfficeRepository;
import com.alom.office_management.response.PersonDTO;
import com.alom.office_management.response.ResponseDTO;


@Service
public class OfficeServiceImpl implements OfficeService {
	
	@Autowired
	private OfficeRepository officeRepository;
//	private RestTemplate restTemplate;
	
	//private OfficeMapper officeMapper;
	/*
	public OfficeServiceImpl(OfficeRepository officeRepository, OfficeMapper officeMapper ) {
		this.officeRepository = officeRepository;
		this.officeMapper = officeMapper;
	}
	*/
	
	@Override
	public Optional<OfficeEntity> getOfficeById(Integer id) {
		Optional<OfficeEntity> officeEntity = officeRepository.findById(id);
		return officeEntity;
	}
	/*
	@Override
	public Optional<OfficeDTO> getOfficeById(Integer id) {
		Optional<OfficeDTO> officeDTO = officeMapper.mapOffice(officeRepository.findById(id));
		return officeDTO;
	}
	*/

	@Override
	public List<OfficeEntity> listAllOffices() {
		List<OfficeEntity> offices = officeRepository.findAll();
		return offices;
	}

	@Override
	public OfficeEntity saveOffice(OfficeEntity officeEntity) {
		officeEntity = officeRepository.save(officeEntity);
		return officeEntity;
	}
	
	@Override
	public OfficeEntity updateOffice(OfficeEntity officeEntity) {
		//officeEntity = officeRepository.merge(officeEntity);
		return null;
	}

	@Override
	public void deteleById(Integer id) {
		officeRepository.deleteById(id);
	}
	
	@Override
	public List<OfficeEntity> findAllOfficeByCreationUser(Long creationUser){
		List<OfficeEntity> offices = officeRepository.findAllOfficeByCreationUser(creationUser);
		return offices;
	}
	
	@Override
	public int countOfficeByfloor(int floor){
		List<OfficeEntity> offices = officeRepository.findAllOfficeByFloor(floor);
		return offices.size();
	}
	
	@Override
	public Boolean existsById(Integer id){
		return officeRepository.existsById(id);
	}
	
	@Override
	public Boolean existsByName(String name) {
		return officeRepository.existsByName(name);
	}
	/*
	@Override
	public ResponseDTO getOfficeByIdMorePerson(Integer id) {
		ResponseDTO responseDTO = new ResponseDTO();
		OfficeEntity officeEntity = new OfficeEntity();
		officeEntity = officeRepository.findById(id).get();
		
		ResponseEntity<PersonDTO> responseEntity = restTemplate.getForEntity("http://localhost:8080/clinic/service/GestionPersona/search/"+officeEntity.getCreationUser(), PersonDTO.class);
		
		PersonDTO personDTO = responseEntity.getBody();
		responseDTO.setOfficeEntity(officeEntity);
		responseDTO.setPersonDTO(personDTO);
		
		return responseDTO;
	}*/
}
