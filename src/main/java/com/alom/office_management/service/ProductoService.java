package com.alom.office_management.service;

import org.springframework.http.ResponseEntity;

import com.alom.office_management.response.ProductoDTO;

public interface ProductoService {
	public ResponseEntity<ProductoDTO> getProductoDTO(Long id);
}
