package com.alom.office_management.service;

import java.util.List;
import java.util.Optional;

import com.alom.office_management.model.OfficeEntity;
import com.alom.office_management.response.ResponseDTO;

public interface OfficeService {
	//public Optional<OfficeDTO> getOfficeById(Integer id);
	public Optional <OfficeEntity> getOfficeById(Integer id);
	public List<OfficeEntity> listAllOffices();
	public OfficeEntity saveOffice(OfficeEntity officeEntity);
	public OfficeEntity updateOffice(OfficeEntity officeEntity);
	public void deteleById(Integer id);
	
	public List<OfficeEntity> findAllOfficeByCreationUser(Long creationUser);
	public int countOfficeByfloor(int floor);
	public Boolean existsById(Integer id);
	public Boolean existsByName(String name);
	//public ResponseDTO getOfficeById(Integer id);
	//public ResponseDTO getOfficeByIdMorePerson(Integer id);
	
}
