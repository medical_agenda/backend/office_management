package com.alom.office_management.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PersonServiceImpl implements PersonService  {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public ResponseEntity<String> getSaludar() {
		
		ResponseEntity<String> responseEntity = restTemplate.getForEntity
				("http://localhost:8080/clinic/service/Saludo/saludar", String.class);

		return responseEntity;
	}
}
