package com.alom.office_management.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.alom.office_management.response.ProductoDTO;

@Service
public class ProductoServiceImpl implements ProductoService{
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public ResponseEntity<ProductoDTO> getProductoDTO(Long id) {
		
		ResponseEntity<ProductoDTO> responseEntity = restTemplate.getForEntity
				("http://localhost:8092/productos/"+id, ProductoDTO.class);

		return responseEntity;
	}
}
