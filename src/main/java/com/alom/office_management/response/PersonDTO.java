package com.alom.office_management.response;


import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonDTO {
	private	Long			id;
	private	String			identificationNumber; 
	private	String			firstName;
	private	String			secondName;
	private	String			fistLastName;
	private	String			secondLastName; 
	private	String			phoneNumber; 
	//private PersonTypeEnum	personTypeEnum;
	private	Long			dTypId;
	private	String			email;
	private	LocalDate		birthDate;
	private	String			userName;
	private	LocalDate		createDate; 
	private	LocalDate		modificationDate;
	private	Boolean			status;
}
