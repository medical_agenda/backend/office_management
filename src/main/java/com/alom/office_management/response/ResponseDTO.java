package com.alom.office_management.response;

import com.alom.office_management.model.OfficeEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDTO {
	private OfficeEntity OfficeEntity;
	private PersonDTO personDTO;
	private ProductoDTO ProductoDTO;
}
