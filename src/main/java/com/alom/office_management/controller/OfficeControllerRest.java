package com.alom.office_management.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.alom.office_management.exception.OfficeNotFoundException;
import com.alom.office_management.model.OfficeEntity;
import com.alom.office_management.response.ProductoDTO;
import com.alom.office_management.service.OfficeService;
import com.alom.office_management.service.PersonService;
import com.alom.office_management.service.ProductoService;

@RestController
@RequestMapping("/office")
public class OfficeControllerRest {
	
	private OfficeService officeService;
	private PersonService personService;
	private ProductoService productoService;
	
	public OfficeControllerRest(OfficeService officeService, PersonService personService, ProductoService productoService) {
		this.officeService= officeService;	
		this.personService= personService;
		this.productoService = productoService;
	}
	
	@GetMapping("/getOfficeById/{id}")
	public ResponseEntity<?> getOfficeById(@PathVariable Integer id){
		System.out.println("getOfficeById: "+id);
		Optional<OfficeEntity> optOfficeEntity = this.officeService.getOfficeById(id);
		
		try {
			OfficeEntity officeEntity = optOfficeEntity.orElseThrow(OfficeNotFoundException::new);
			return ResponseEntity.ok(officeEntity);
		}catch(OfficeNotFoundException e){
			return ResponseEntity.notFound().build();
			//return ResponseEntity.badRequest(e);
		}
	}
	
	@GetMapping("/getAllOffices")
	public ResponseEntity<?> listAllOffices(){
		System.out.println("listAllOffices");
		List<OfficeEntity> list = officeService.listAllOffices();
		return ResponseEntity.ok(list);
	}

	@PostMapping
	public ResponseEntity<?> saveOffice(@RequestBody OfficeEntity officeEntity ){
		
		if( !this.officeService.existsByName(officeEntity.getName()) && this.officeService.countOfficeByfloor(officeEntity.getFloor())<3 ) {
		//if(!this.officeService.existsByName(officeEntity.getName())){
		
			officeEntity = officeService.saveOffice(officeEntity);
			
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(officeEntity.getId()).toUri();		
			return ResponseEntity.created(location).build();
		}
		else {
			return ResponseEntity.ok(false);	
		}
		//return ResponseEntity.ok(null);	
	}
	
	@PutMapping 
	public ResponseEntity<?> updateOffice(@RequestBody OfficeEntity officeEntity){
		
		return ResponseEntity.ok(officeEntity);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deteleById(@PathVariable Integer id){
		officeService.deteleById(id);
		return ResponseEntity.ok(null);
	}
	
	@GetMapping("/user/{creationUser}")
	public ResponseEntity<?> findAllOfficeByCreationUser(@PathVariable Long creationUser){
		
		List<OfficeEntity> list = this.officeService.findAllOfficeByCreationUser(creationUser);
		return ResponseEntity.ok(list);
	}
	
	
	//if( !this.officeService.existsByName(officeEntity.getName()) && this.officeService.countOfficeByfloor(officeEntity.getFloor())<3 )
	@GetMapping("/cantidad/{floor}")
	public ResponseEntity<?> countOfficeByfloor(@PathVariable int floor){
		
		int count = this.officeService.countOfficeByfloor(floor);
		return ResponseEntity.ok(count);
	}
	
	@GetMapping("/id/{id}")
	public ResponseEntity<?> existsById(@PathVariable Integer id){
		return ResponseEntity.ok(this.officeService.existsById(id));
	}
	
	@GetMapping("/name/{name}")
	public ResponseEntity<?> existsByName(@PathVariable String name){
		return ResponseEntity.ok(this.officeService.existsByName(name));
	}
	/*
	@GetMapping("/and/{id}")
	public ResponseEntity<?> getOfficeByIdMorePerson(@PathVariable Integer id) {
		return officeService.getOfficeByIdMorePerson(id);
	}*/
	
	@GetMapping("/hola")
	public ResponseEntity<?> getSaludar(){
		return ResponseEntity.ok(this.personService.getSaludar());
	}
	
	@GetMapping("/producto/{id}")
	public ResponseEntity<ProductoDTO> getProductoDTO(@PathVariable Long id){
		return this.productoService.getProductoDTO(id);
	}
}
